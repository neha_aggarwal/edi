class ProductsController < ApplicationController
  

  def generate_edi
  	edi_file  = params[:edi]
    if edi_file.present?
    	ik5_arr = []
    	edi_elem = edi_file.split("~")
    	edi_elem.each do |a|
    		bat_num = a.split("*")
    		if a.match(/\b[A][K][2]\b/)
          h1={}
          h1[:batch_number] = bat_num[2]
    			ik5_arr << h1
    		elsif a.match(/\b[I][K][5]\b/)
    	    ik5_hash = {}
    			bat_num.drop(1).each_with_index do |e, index|
	    			if index ==0 
	            IK5_FIRST_LEVEL.each do |k,v|
	            	if e==k
	            		ik5_hash["#{e}"] = v
			          end
	            end
	          elsif index==1
	          	IK5_SECOND_LEVEL.each do |r, m|
		        		if r == e
		        			ik5_hash["#{e}"] = m
		        		end
		        	end
	        	end
		      end
		      ik5_arr << ik5_hash
		      unless ik5_hash.present?
	    	    render status: :unprocessable_entity, json: {error: 'IK5 is not found'}
		      end
		    end
		  end
		  if ik5_arr.present?
	      render status: :ok, json: {IK5: ik5_arr}
      else
	    	render status: :unprocessable_entity, json: {error: 'Data is not found'}
	    end
    else
    	render status: :unprocessable_entity, json: {error: 'EDI File is not available'}
    end
  end

  
end
